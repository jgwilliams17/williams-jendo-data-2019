close all; 
clear all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex'); 

%% PARAMETERS:

R = 0.125; % cm (radius of tubing)
mu = 0.01; % g/cm*s (viscosity of water at 20 C
rho = 1; % g/cm^3 (density of water)
g = 981; % cm/s^2 (gravitational acceleration)
a = 0.06; % cm (working channel radius)
L = 79; % cm (length of scope working channel)

%% POISEUILLE FORMULA:

h_vec =linspace(50,120,1000);
p_inlet_no_vd = rho*g*h_vec;
sigma = (h_vec/L)*(a/R)^4;
p_inlet_vd = rho*g*h_vec.*(1-sigma./(1+sigma));

Q_poiseuille = @(pinlet) (pi*pinlet*a^4/(8*L*mu));

no_vd = plot(h_vec,Q_poiseuille(p_inlet_no_vd),'k--','Linewidth',2);
hold on
vd = plot(h_vec,Q_poiseuille(p_inlet_vd),'k-','Linewidth',2);

%% PLOT DATA:

load('Fig2.mat');
data = errorbarxy(h(1),Q(1),2,err(1),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(h(2),Q(2),2,err(2),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(h(3),Q(3),2,err(3),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(h(4),Q(4),2,err(4),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(h(5),Q(5),2,err(5),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);

%% GRAPH LABELS

xlabel('Head Height (cm)','Fontsize',14,'Interpreter','latex');
ylabel('Volumetric Flow Rate (cm$^3$/s)','Fontsize',14,'Interpreter','latex');
title('Volumetric Flow Rate vs. Head Height','Fontsize',20,'Interpreter','latex')
grid on
legend([data(1) no_vd vd],{'Data','Hydrostatic Pressure Prediction','Viscous Dissipation Prediction'},'Location','northwest');
set(gca,'Fontsize',14);
set(legend,'Fontsize',18);