clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');

%% LOAD DATA:

load('FigS3c.mat'); 

data = [mass_no_tool; mass_flexiva_200; mass_flexiva_365; mass_optiflex_15; mass_optiflex_19; mass_zipwire];

% Loads mass measurement data for each tool. Columns correspond to runs of
% the same experiment, with rows corresponding to time intervals (in
% seconds). Combines all six data sets into one matrix - "data".

%% PLOT RAW DATA:

% Plot the raw mass measurements (g) over time:

c=get(gca,'ColorOrder');
figure(1)
ln(1:3)=plot(mass_no_tool','Color',c(1,:),'Linewidth',2);
hold on
ln(4:6)=plot(mass_flexiva_200','Color',c(2,:),'Linewidth',2);
ln(7:9)=plot(mass_flexiva_365','Color',c(3,:),'Linewidth',2);
ln(10:12)=plot(mass_optiflex_15','Color',c(4,:),'Linewidth',2);
ln(13:15)=plot(mass_optiflex_19','Color',c(5,:),'Linewidth',2);
ln(16:18)=plot(mass_zipwire','Color',c(6,:),'Linewidth',2);

legend([ln(1) ln(4) ln(7) ln(10) ln(13) ln(16)],{'No Tool','Flexiva Laser Fibre 200','Flexiva Laser Fibre 365','Optiflex Basket 1.5 F','Optiflex Basket 1.9 F','ZIPwire 0.038 in.'},'Location','Northwest');
set(legend,'fontsize',20);
set(gca,'fontsize',20);
grid on
title('Raw Data - Working Tool','Fontsize',24);
xlabel('Time (s)','Fontsize',20);
ylabel('Mass (g)','Fontsize',20);

%% APPROXIMATING FLOW RATE & ERRORS:

total_time = 60;
Q = zeros(6,1);
std_dev = zeros(6,1);

% Calculates mean flow rate and standard devation for each tool data set:

for i = 1:5
    Q(i) = sum(data(i*3-2:i*3,end)/total_time)/3;
    std_dev(i) = std(data(i*3-2:i*3,end)/60);
end

% For zipwire we calculate differently - flow so low we take flow rate to
% be 1 g/(time taken to go from 1 g to 2 g):

Q(6)=sum(1./sum((data(16:18,:)==1),2))/3;
std_dev(6) = std(1./sum((data(16:18,:)==1),2));
    
% Calculates the systematic error in the measurement due to accuracy of
% balance:

error_in_final_measurement = 0.5; % Accuracy of balance is 1 g, so final measurement is +/- 0.5 g.
error_in_flow_rate = sqrt(2)*error_in_final_measurement/total_time; % have sqrt(2) b.c. mass is calculated as difference

systematic_error = sqrt(3*error_in_flow_rate^2)/3;

err = sqrt(std_dev.^2+systematic_error^2);

%% OUTPUT
fprintf('--------------------\n');
fprintf(['No Tool: %g ' char(177) ' %g g/s\n'],Q(1),err(1));
fprintf(['Flexiva 200: %g ' char(177) ' %g g/s\n'],Q(2),err(2));
fprintf(['Flexiva 365: %g ' char(177) ' %g g/s\n'],Q(3),err(3));
fprintf(['Optiflex 1.5 F: %g ' char(177) ' %g g/s\n'],Q(4),err(4));
fprintf(['Optife 1.9 F: %g ' char(177) ' %g g/s\n'],Q(5),err(5));
fprintf(['ZIPwire 0.038 in.: %g ' char(177) ' %g g/s\n'],Q(6),err(6));
fprintf('--------------------\n');