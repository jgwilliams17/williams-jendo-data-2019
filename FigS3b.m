clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');

%% LOAD DATA:

load('FigS3b.mat'); 

% Loads mass measurement data for each tool. Columns correspond to runs of
% the same experiment, with rows corresponding to time intervals (in
% seconds). Combines all six data sets into one matrix - "data".

%% PLOT RAW DATA:

% Plot the raw mass measurements (g) over time:

c=get(gca,'ColorOrder');
figure(1)
ln1=plot(R1_data','Color',c(1,:),'Linewidth',2);
hold on
ln2=plot(R2_data','Color',c(2,:),'Linewidth',2);
ln3=plot(R3_data','Color',c(3,:),'Linewidth',2);
ln4=plot(R4_data','Color',c(4,:),'Linewidth',2);
ln5=plot(R5_data','Color',c(5,:),'Linewidth',2);

legend([ln1(1) ln2(1) ln3(1) ln4(1) ln5(1)],{'$R_{curv} = 5.4$ cm','$R_{curv} = 2.8$ cm','$R_{curv} = 1.6$ cm','$R_{curv} = 1.3$ cm','$R_{curv} = 1.1$ cm'},'Location','Northwest');
set(legend,'fontsize',20);
grid on
title('Raw Data - Deflected Scope','Fontsize',24);
set(gca,'fontsize',20);
xlabel('Time (s)','Fontsize',20);
ylabel('Mass (g)','Fontsize',20);

%% APPROXIMATING FLOW RATE & ERRORS:

Q = zeros(5,1);
std_dev = zeros(5,1);
total_time = 60;

R = [R1; R2; R3; R4; R5];

% Calculates mean flow rate and standard devation for each tool data set:

Q(1) = (sum(max(R1_data'))/total_time)/size(R1_data,1);
Q(2) = (sum(max(R2_data'))/total_time)/size(R2_data,1);
Q(3) = (sum(max(R3_data'))/total_time)/size(R4_data,1);
Q(4) = (sum(max(R4_data'))/total_time)/size(R4_data,1);
Q(5) = (sum(max(R5_data'))/total_time)/size(R5_data,1);

std_dev(1) = std(max(R1_data')/total_time);
std_dev(2) = std(max(R2_data')/total_time);
std_dev(3) = std(max(R3_data')/total_time);
std_dev(4) = std(max(R4_data')/total_time);
std_dev(5) = std(max(R5_data')/total_time);
    
% Calculates the systematic error in the measurement due to accuracy of
% balance:

error_in_final_measurement = 0.5; % Accuracy of balance is 1 g, so final measurement is +/- 0.5 g.

systematic_error = sqrt(3*(sqrt(2)*error_in_final_measurement/total_time)^2)/3;

err = sqrt(std_dev.^2+systematic_error^2);

%% OUTPUT
fprintf('--------------------\n');
fprintf(['R = %1.1f cm: %g ' char(177) ' %g g/s\n'],mean(R1),Q(1),err(1));
fprintf(['R = %1.1f cm: %g ' char(177) ' %g g/s\n'],mean(R2),Q(2),err(2));
fprintf(['R = %1.1f cm: %g ' char(177) ' %g g/s\n'],mean(R3),Q(3),err(3));
fprintf(['R = %1.1f cm: %g ' char(177) ' %g g/s\n'],mean(R4),Q(4),err(4));
fprintf(['R = %1.1f cm: %g ' char(177) ' %g g/s\n'],mean(R5),Q(5),err(5));
fprintf('--------------------\n');