clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex'); 

%% PARAMETERS

L = 79; % total length of the scope (cm)
L_c = 5.7; % length of the curved scope portion (cm)
L_s = L - L_c;
rho = 1; % density (g/cm^3)
g = 981; % acceleration due to gravity (cm/s^2)
a = 0.06; % radius of the working channel (cm)
mu = 0.01; % g/cm*s (viscosity of water at 20 C)
nu = mu/rho; % kinematic viscosity
R = 0.125;

D = @(P_mid,delta,P_inlet) (P_mid/L_c)*a^3*sqrt(2*delta)/(mu*nu); % Dean Number

%% OPTIONS

N = 100; % Number of delta values to calculate prediction for.

minimum_rc = 1; % minimum radius of curvature (cm)
maximum_rc = 100;
curv = linspace(1/maximum_rc,1/minimum_rc,N);

options = optimoptions('fsolve','TolFun',1e-16); % Set tolerance of fsolve function.

%% SET HEAD HEIGHTS

h = 83; % Height of bucket during experiments.
h_min = h-1; % Include error on height.
h_max = h+1;

%% CALCULATE FLUX (DEAN EQUATION) - VARY CURVATURE

Q_curved_dean = zeros(N,1);
P_mid_dean = zeros(N,1);
P_inlet_dean = zeros(N,1);
Ds_dean = zeros(N,1);

for h = [h_min h_max]
    P_inlet_guess = rho*g*h; % inlet pressure (head height)
    P_mid_guess = (P_inlet_guess)/(L/L_c);
    for i = 1:N
        delta = a*curv(i);
        
        % Solve two equations for P_mid & P_inlet:
        
        Dean = @(P) [(P(2)/L_c).*(1 - 0.0306*(P(2).*sqrt(2*delta)*a^3./(96*mu*nu*L_c)).^4) - (P(1) - P(2))./L_s; pi*R^4/(8*mu)*(rho*g-P(1)/h)-(pi*a^4*(P(1)-P(2)))/(8*L_s*mu)];
        P0 = [P_inlet_guess P_mid_guess]; % guess
        P = fsolve(Dean,P0,options);
        
        % Store values:
        
        P_inlet_dean(i) = P(1);
        P_mid_dean(i) = P(2);
        
        % Update solver guesses:
    
        P_mid_guess = P(2);
        P_inlet_guess = P(1);
        
        % Store flux & dean number:
        Q_curved_dean(i) = (pi*a^4*(P_inlet_dean(i) - P_mid_dean(i)))/(8*L_s*mu);
        Ds_dean(i) = D(P_mid_dean(i),delta,P_inlet_dean(i));
    end

    if h==h_min
    mini = plot(a*curv(Ds_dean <= 96),Q_curved_dean(Ds_dean <= 96),'k--');
    hold on
    x_lower = a*curv(Ds_dean <= 96);
    y1_lower = Q_curved_dean(Ds_dean <= 96);
    end
    
    if h==h_max
    max = plot(a*curv(Ds_dean <= 96),Q_curved_dean(Ds_dean <= 96),'k--');
    hold on
    y2_lower = Q_curved_dean(Ds_dean <= 96);
    end

    %% CALCULATE FLUX (COLLINS DENNIS EQUATION) - VARY CURVATURE

    Q_curved_cd = zeros(N,1);
    P_mid_cd = zeros(N,1);
    P_inlet_cd = zeros(N,1);
    Ds_cd = zeros(N,1);

    P_mid_guess = P_mid_dean(1);
    P_inlet_guess = P_inlet_dean(1);

    for i = 1:N
        delta = a*curv(i);

        Collins_Dennis = @(P) [(8.12*(P(2)*a^3*sqrt(2*delta)/(mu*nu*L_c)).^(-1/3) - 16.7*(P(2)*a^3*sqrt(2*delta)/(mu*nu*L_c)).^(-2/3))*(P(2)/L_c) - ((P(1) - P(2))/L_s); pi*R^4/(8*mu)*(rho*g-P(1)/h)-(pi*a^4*(P(1) - P(2)))/(8*L_s*mu)];
        
        P0 = [P_inlet_guess P_mid_guess]; % guess
        P = fsolve(Collins_Dennis,P0,options);
        
        P_inlet_cd(i) = P(1);
        P_mid_cd(i) = P(2);

        P_mid_guess = P(2);
        P_inlet_guess = P(1);
 
        Q_curved_cd(i) = (pi*a^4*(P_inlet_cd(i) - P_mid_cd(i)))/(8*L_s*mu);
        Ds_cd(i) = D(P_mid_dean(i),delta,P_inlet_cd(i));
    end
    
    if h==h_min
    mini = plot(a*curv(Ds_cd >= 96),Q_curved_cd(Ds_cd >= 96),'k--');
    x_higher = a*curv(Ds_cd >= 96);
    y1_higher = Q_curved_cd(Ds_cd >= 96);
    end
    
    if h==h_max
    max = plot(a*curv(Ds_cd >= 96),Q_curved_cd(Ds_cd >= 96),'k--');
    y2_higher = Q_curved_cd(Ds_cd >= 96);
    end

end

%% Fill revion between curves for max & min heights:

x=[x_lower x_higher];                  
y1=[y1_lower; y1_higher];                   
y2=[y2_lower; y2_higher];                 
X=[x,fliplr(x)];                
Y=[y1; flipud(y2)];              
pred = fill(X,Y, [0.9 0.9 0.9]);  
set(pred,'LineWidth',3)

%% PLOT DATA

load('Fig3.mat')

d = a./R;
data = errorbarxy(mean(d(1,:)),Q(1),std(d(1,:)),err(1),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
hold on
errorbarxy(mean(d(2,:)),Q(2),std(d(2,:)),err(2),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(mean(d(3,:)),Q(3),std(d(3,:)),err(3),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(mean(d(4,:)),Q(2),std(d(4,:)),err(4),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(mean(d(5,:)),Q(5),std(d(5,:)),err(5),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);

%% GRAPH LABELS

xlabel('$a/R_{curv}$ (dimensionless)','Fontsize',14,'Interpreter','latex');
ylabel('Volumetric Flow Rate (cm$^3$/s)','Fontsize',14,'Interpreter','latex');
title('Volumetric Flow Rate vs. Tip Curvature Ratio','Fontsize',20,'Interpreter','latex')
grid on
legend([data(1) pred],{'Data','Prediction'});
set(gca,'Fontsize',14);
set(legend,'Fontsize',18);
