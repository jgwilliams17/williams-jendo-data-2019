close all; 
clear all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex'); 

%% PARAMETERS:

R = 0.125; % cm (radius of tubing)
mu = 0.01; % g/cm*s (viscosity of water at 20 C
h = 83; % cm (height of fluid in irrigation bag)
height = [h-1 h h+1];
rho = 1; % g/cm^3 (density of water)
g = 981; % cm/s^2 (gravitational acceleration)
a = 0.06; % cm (working channel radius)
L = 79; % cm (length of scope working channel)

r_no_tool = 1e-16; % NO WORKING TOOL
r_flexiva_200 = 0.02215; % FLEXIVA LASER FIBER 200
r_optiflex_13 = 1.3/60; % OPTIFLEX 13
r_flexiva_365 = 0.0302; % FLEXIVA LASER FIBER 365
r_optiflex_19 = 1.9/60; % OPTIFLEX 19 % Edit 15/1/19: This is ZeroTip 1.9 F.
r_zipwire = 0.0483; % ZIPWIRE 0.038 in

%% FLOW THROUGH TUBING

Q_tubing = @(pinlet,h) pi*R^4/(8*mu)*(rho*g-pinlet/h);

%% CONCENTRIC ANALYTICAL SOLUTION:

Q_concentric = @(r_wt,pinlet) (pi*pinlet/(8*L*mu)).*(a^2 - r_wt.^2).*(a^2 + r_wt.^2 - ((a^2 - r_wt.^2)./(log(a./r_wt))));

%% COMSOL Results:

load('Fig5Ellipse.mat');
r=rwt;
ellipse_sweep=Q; % flow rates for ellipses with rows for different eccentricity values (e) & columns for different tool sizes (b)
circle_max_offset = zeros(length(r),1);
circle_max_offset_with_vd = zeros(length(r),3);

for i = 1:length(r)
    circle_max_offset(i)=ellipse_sweep(end,i);
end

% Include Viscous Dissipation:

C_offset = circle_max_offset./(rho*g*h);

for i = 1:3
    P_inlet_offset = pi*R^4*rho*g*height(i)./(C_offset*8*mu*height(i)+pi*R^4);
    circle_max_offset_with_vd(:,i) = Q_tubing(P_inlet_offset,height(i));
end

%% Analytical Concentric Results:

C_concentric = Q_concentric(r,1);
Circle_Concentric_with_VD = zeros(length(r),3);

% Include Viscous Dissipation:

for i = 1:3
    P_inlet_concentric = pi*R^4*rho*g*height(i)./(C_concentric*8*mu*height(i)+pi*R^4);
    Circle_Concentric_with_VD(:,i) = Q_concentric(r,P_inlet_concentric);
end

%% Fill Region Between Concentric & Offset:

x = [2*r*10];                  
y1 = [Circle_Concentric_with_VD(:,2)'];                     
y2 = [circle_max_offset_with_vd(:,2)'];                  
X=[x,fliplr(x)];                
Y=[y1,fliplr(y2)];            
pred = fill(X,Y, [0.9 0.9 0.9]);  
set(pred,'LineWidth',3)
hold on

%% Plot Ellipse Predictions

k=0;
for i = [2 50]
    curr=ellipse_sweep(i,:);
    C = curr./(rho*g*h);
    P_inlet = pi*R^4*rho*g*h./(C*8*mu*h+pi*R^4);
    curr = Q_tubing(P_inlet,h);
    k=k+1;
    el_plot(k)=plot(20*r(curr~=0),curr(curr~=0),'Linewidth',2);
end

%% PLOT DATA

load('Fig5Circle.mat');

errorbarxy(20*r_no_tool,Q(1),0,err(1),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(20*r_flexiva_200,Q(2),0,err(2),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(20*r_flexiva_365,Q(3),0,err(3),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(20*r_optiflex_13,Q(4),0,err(4),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
errorbarxy(20*r_optiflex_19,Q(5),0,err(5),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);
data=errorbarxy(20*r_zipwire,Q(6),0,err(6),'Color','r','LineStyle','none','Marker','x','MarkerFaceColor','w','Linewidth',2,'MarkerSize',10);


%% GRAPH LABELS

xlabel('Working Tool Diameter (mm)','Fontsize',14,'Interpreter','latex');
ylabel('Volumetric Flow Rate (cm$^3$/s)','Fontsize',14,'Interpreter','latex');
title('Volumetric Flow Rate vs. Tool Size','Fontsize',20,'Interpreter','latex');
grid on
% legend([data(1) pred],{'Data','Circular Channel'});
set(gca,'Fontsize',14);
set(legend,'Fontsize',18)

%% PLOT Q vs. E

area = 0.06^2*pi;
a = area./(pi*b);
e = sqrt(1-(b./a).^2);

figure
plot(e,ellipse_sweep(:,100),'k','Linewidth',2);
set(gca,'Fontsize',14);
xlim([min(e) max(e)]);
xlabel('Ellipse Eccentricity','Fontsize',14,'Interpreter','latex');
title('Volumetric Flow Rate vs. Channel Shape','Fontsize',20,'Interpreter','latex');
grid on
ylabel('Volumetric Flow Rate (cm$^3$/s)','Fontsize',14,'Interpreter','latex');