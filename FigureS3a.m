clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');

%% LOAD DATA:

load('FigS3a.mat'); 

% Loads mass measurement data for each tool. Columns correspond to runs of
% the same experiment, with rows corresponding to time intervals (in
% seconds). Combines all six data sets into one matrix - "data".

H1_data = H1_data_full(:,1:60);
H2_data = H2_data_full(:,1:60);
H3_data = H3_data_full(:,1:60);
H4_data = H4_data_full(:,1:60);
H5_data = H5_data_full(:,1:60);

%% PLOT RAW DATA:

% Plot the raw mass measurements (g) over time:

c=get(gca,'ColorOrder');
figure(1)
ln1=plot(H1_data','Color',c(1,:),'Linewidth',2);
hold on
ln2=plot(H2_data','Color',c(2,:),'Linewidth',2);
ln3=plot(H3_data','Color',c(3,:),'Linewidth',2);
ln4=plot(H4_data','Color',c(4,:),'Linewidth',2);
ln5=plot(H5_data','Color',c(5,:),'Linewidth',2);

legend([ln1(1) ln2(1) ln3(1) ln4(1) ln5(1)],{'$h = 57$ cm','$h = 67$ cm','$h = 83$ cm','$h = 98.5$ cm','$h = 112.5$ cm'},'Location','Northwest');
set(legend,'fontsize',20);
set(gca,'fontsize',20);
grid on
title('Raw Data - Straight Scope','Fontsize',24);
xlabel('Time (s)','Fontsize',20);
ylabel('Mass (g)','Fontsize',20);

%% APPROXIMATING FLOW RATE & ERRORS:

Q = zeros(5,1);
std_dev = zeros(5,1);
T = 60; % total time

% Calculates mean flow rate and standard devation for each tool data set (first 60 seconds of data):

Q(1) = (sum(max(H1_data'))/T)/size(H1_data,1);
Q(2) = (sum(max(H2_data'))/T)/size(H2_data,1);
Q(3) = (sum(max(H3_data'))/T)/size(H3_data,1);
Q(4) = (sum(max(H4_data'))/T)/size(H4_data,1);
Q(5) = (sum(max(H5_data'))/T)/size(H5_data,1);

std_dev(1) = std(max(H1_data')/T);
std_dev(2) = std(max(H2_data')/T);
std_dev(3) = std(max(H3_data')/T);
std_dev(4) = std(max(H4_data')/T);
std_dev(5) = std(max(H5_data')/T);

% Calculates the systematic error in the measurement due to accuracy of
% balance:

error_in_final_measurement = 0.5; % Accuracy of balance is 1 g, so final measurement is +/- 0.5 g.

systematic_error = sqrt(3*(sqrt(2)*error_in_final_measurement/T)^2)/3*ones(5,1);

%%

% If don't truncate data to 60 seconds, can use below to calculate flow
% rates:

%Q(1) = (sum(max(H1_data'))/100)/size(H1_data,1);
%Q(2) = (sum(max(H2_data'))/100)/size(H2_data,1);
%Q(3) = (sum(max(H3_data(1:3,:)')/100)+sum(max(H3_data(4:6,:)'))/60)/size(H3_data,1);
%Q(4) = (sum(max(H4_data'))/100)/size(H4_data,1);
%Q(5) = (sum(max(H5_data(1:3,:)')/300)+sum(max(H5_data(4:6,:)'))/100+sum(max(H5_data(7:end,:)'))/60)/size(H5_data,1);

%std_dev(1) = std(max(H1_data')/100);
%std_dev(2) = std(max(H2_data')/100);
%std_dev(3) = std([max(H3_data(1:3,:)')/100 max(H3_data(4:6,:)')/60]);
%std_dev(4) = std(max(H4_data')/100);
%std_dev(5) = std([max(H5_data(1:3,:)')/300 max(H5_data(4:6,:)')/100 max(H5_data(7:end,:)')/60]);
    
% Calculates the systematic error in the measurement due to accuracy of
% balance:

% error_in_final_measurement = 0.5; % Accuracy of balance is 1 g, so final measurement is +/- 0.5 g.
% 
% systematic_error_1_2_4 = sqrt(3*(error_in_final_measurement/100)^2)/3;
% systematic_error_3 = sqrt(3*(error_in_final_measurement/100)^2+3*(error_in_final_measurement/60)^2)/6;
% systematic_error_5 = sqrt(3*(error_in_final_measurement/300)^2+3*(error_in_final_measurement/100)^2+8*(error_in_final_measurement/60)^2)/14;
% 
% systematic_error = [systematic_error_1_2_4 systematic_error_1_2_4 systematic_error_1_2_4 systematic_error_1_2_4 systematic_error_5];

%%
err = sqrt(std_dev.^2+systematic_error.^2);

%% OUTPUT
fprintf('--------------------\n');
fprintf(['Head Height %1.1f cm: %g ' char(177) ' %g g/s\n'],h(1),Q(1),err(1));
fprintf(['Head Height %1.1f cm: %g ' char(177) ' %g g/s\n'],h(2),Q(2),err(2));
fprintf(['Head Height %1.1f cm: %g ' char(177) ' %g g/s\n'],h(3),Q(3),err(3));
fprintf(['Head Height %1.1f cm: %g ' char(177) ' %g g/s\n'],h(4),Q(4),err(4));
fprintf(['Head Height %1.1f cm: %g ' char(177) ' %g g/s\n'],h(5),Q(5),err(5));
fprintf('--------------------\n');